package historias

class HistoriaController {

	static scaffold = Historia

	def nuevahistoria(){
		new Historia(
			autor: params.autor,
			texto: params.texto,
			fecha: new Date(),
			lat: params.lat,
			lng: params.lng,
			fotoid: params.fotoid).save()
		render("OK")
	}
}
